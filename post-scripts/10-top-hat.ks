firstboot --reconfig

%post --nochroot
pkg_DIR = $ANA_INSTALL_PATH/var/cache/top-hat-pkg/

# NVIDIA
if [[ $(lsmod | grep nvidia) ]]; then # Searches for i2c_nvidia_gpu
	echo "Installing Nvidia drivers"
	dnf install $pkg_DIR/nvidia/* --installroot=$ANA_INSTALL_PATH
fi

%end
